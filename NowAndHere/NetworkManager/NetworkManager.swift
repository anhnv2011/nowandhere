//
//  NetworkManager.swift
//  NowAndHere
//
//  Created by Nguyen Viet Anh on 2/28/23.
//

import Foundation

struct Constant {
    static let baseUrl = "https://devapi.kulpick.com/api/v1/"
    
}

class NetworkManager {
    
    
}

class Observable<T> {
    var value: T? {
        didSet {
            
        }
    }
    
    init(_ value: T?) {
        self.value = value
    }
    
    private var listener: ((T?) -> Void)?
    
    func bind(listener: @escaping (T?) -> Void) {
        listener(value)
        self.listener = listener
        
    }
}
